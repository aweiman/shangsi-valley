/*
路由器模块 
 */
import Vue from 'vue'
import VueRouter from 'vue-router'

import About from '../views/About.vue'
import Home from '../views/Home.vue'
import News from '../views/News.vue'
import Message from '../views/Message.vue'
import MessageDetail from '../views/MessageDetail.vue'
Vue.use(VueRouter);

export default new VueRouter({
    //n个路由
    routes:[
        {
            path: '/about',//About路径变成小写的
            component:About,//注意不需要引号括起来
        },
        {
            path: '/home',
            component:Home,
            children:[
               {
                //path:'/news',//path最左侧的/永远代表根路由，不对
                path:'/home/news',
                component:News,
               },
               {
                path:'message',//简化写法
                component:Message,
                children:[
                   {
                    path:'/home/message/detail/:id',
                    component:MessageDetail,
                   }
                ]
               },
               {//Home路由下默认显示News路由
                path:'',//表示/home
                redirect:'/home/news',           
                 }
            ]
        },
        {//默认显示路由
            path:'/',
            redirect:'/about',          
        }
    ]
})
