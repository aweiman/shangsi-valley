import Vue from 'vue'
import App from './App.vue'
// 引入base.css
import './base.css'

Vue.config.productionTip = false
new Vue({
    el: '#app',
    components: {App},
    template: '<App/>'
})