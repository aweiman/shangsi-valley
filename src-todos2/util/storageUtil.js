/*
使用localStorage存储数据的模块
1.函数一个功能
2.对象 可以包含多个功能
3.需要向外暴露一个功能还是多个功能
 */
// 因为todos_key是一致的，所以写成常量不易出错
const TODOS_KEY ='todos_key';
export default{
    //保存todos即/将todos最新的值的json，保存到localStorage
    saveTodos(todos){
        window.localStorage.setItem('TODOS_KEY',JSON.stringify(todos));//注意这里参数是todos
    },

    //从localStorage读取todos
    readTodos(){
        return JSON.parse(window.localStorage.getItem('TODOS_KEY') || '[]');
    }
}